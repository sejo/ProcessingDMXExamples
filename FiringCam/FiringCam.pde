import processing.video.*;
import oscP5.*;
import netP5.*;

Capture video;

color [] prevFrame;
int numPixels;
float targetMix;


OscP5 oscP5;
NetAddress myRemoteLocation;

int qlcInputPort;
int qlcOutputPort;

String [] rgbAddr = new String[6];

color rgb;
color rgb2;
color c1;
color c2;

float mix;

boolean doSequenceUp = false;


void setup() {
  qlcInputPort = 9000;
  qlcOutputPort = 7700;
  size(640,480);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,qlcInputPort);
  myRemoteLocation = new NetAddress("127.0.0.1",qlcOutputPort);

  rgbAddr[0] = "/1/fader1";
  rgbAddr[1] = "/1/fader2";
  rgbAddr[2] = "/1/fader3";
  rgbAddr[3] = "/2/fader4";
  rgbAddr[4] = "/2/fader5";
  rgbAddr[5] = "/2/fader6";

  rgb = color(0,0,0);
  rgb2 = color(0,0,0);

  c1 = color(50,20,10);
  c2 = color(220,180,0);

  mix = 0;

  String[] cameras = Capture.list();
  printArray(cameras);
//  video = new Capture(this,cameras[0]);
//  video = new Capture(this,width,height);
  video = new Capture(this,width,height,"/dev/video1");
  video.start();

  println(video.width+" "+video.height);



  numPixels = video.width*video.height;
  prevFrame = new color[numPixels];


 targetMix = 0; 

}


void draw() {
  background(rgb);  

  if(video.available()){
	  video.read();
	  video.loadPixels();
	  float sum = 0;
	  int dif = 0;
	  for(int i=0; i<numPixels; i++){
		color curr = video.pixels[i];	
		color prev = prevFrame[i];

		dif = int(abs(red(curr)-red(prev)) + abs(green(curr)-green(prev)) + abs(blue(curr)-blue(prev)));
		sum += dif/(255.0*255*255);

		prevFrame[i] = curr;


	  }
// 	 println(sum*0.5);
	 targetMix = constrain(sum-0.2,0,1);
	  video.updatePixels();


  }

  if(doSequenceUp){
	  if(mix>=1.0){
		doSequenceUp = false;
		mix = 0;
	  }
	  else{
		rgb = lerpColor(c1,c2,mix);
		updateRGB();
	  	mix += 0.005;
	  }
  }

  else{
//	mix = map(mouseX,0,width,0,1);
	mix = lerp(mix,targetMix,0.1);
	println(mix*100);
	rgb = lerpColor(c1,c2,mix);
	rgb2 = lerpColor(c1,c2,mix);
	updateRGB();
  }
}

void mousePressed() {
	doSequenceUp = true;
}


// Send OSC messages according to rgb data
void updateRGB(){

  OscMessage myMessage = new OscMessage(rgbAddr[0]);
  myMessage.add(map(red(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[1]);
  myMessage.add(map(green(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[2]);
  myMessage.add(map(blue(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[3]);
  myMessage.add(map(red(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[4]);
  myMessage.add(map(green(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[5]);
  myMessage.add(map(blue(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

}


/* incoming osc message are forwarded to the oscEvent method. 
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage 
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());
  println(" typetag: "+theOscMessage.get(0).floatValue());
}
*/
