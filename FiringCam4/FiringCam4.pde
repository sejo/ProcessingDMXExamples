import processing.video.*;
import oscP5.*;
import netP5.*;

Capture[] video;

color [][] prevFrame;
int numPixels;

int videoWidth,videoHeight;


OscP5 oscP5;
NetAddress myRemoteLocation;

int qlcInputPort;
int qlcOutputPort;

String [] rgbAddr;


int N = 4; // Number of fixtures
int NCAM = 2; // Number of cameras

color [] rgb;
float [] mix;
float[] targetMix;

color c1;
color c2;


boolean doSequenceUp = false;


void setup() {
  qlcInputPort = 9000;
  qlcOutputPort = 7700;
  size(640,480);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,qlcInputPort);
  myRemoteLocation = new NetAddress("127.0.0.1",qlcOutputPort);

  rgbAddr = new String[N*3];
  rgbAddr[0] = "/1/fader1";
  rgbAddr[1] = "/1/fader2";
  rgbAddr[2] = "/1/fader3";
  rgbAddr[3] = "/2/fader4";
  rgbAddr[4] = "/2/fader5";
  rgbAddr[5] = "/2/fader6";

  rgbAddr[6] = "/3/fader1";
  rgbAddr[7] = "/3/fader2";
  rgbAddr[8] = "/3/fader3";
  rgbAddr[9] = "/3/fader4";
  rgbAddr[10] = "/3/fader5";
  rgbAddr[11] = "/3/fader6";

  rgb = new color[N];
  mix = new float[N];
  targetMix = new float[N];

  for(int i=0; i<N; i++){
  	rgb[i] = color(0,0,0);
  	mix[i] = 0;
  	targetMix[i] = 0;
  }

  c1 = color(50,20,10);
  c2 = color(250,210,0);


  String[] cameras = Capture.list();
  printArray(cameras);
//  video = new Capture(this,cameras[0]);
//  video = new Capture(this,width,height);

  video = new Capture[2];


  videoWidth = 320;
  videoHeight = 240;
  video[0] = new Capture(this,videoWidth,videoHeight,"/dev/video1");
  video[1] = new Capture(this,videoWidth,videoHeight,"/dev/video2");

  for(int i=0; i<NCAM; i++){
  	video[i].start();
  	println(video[i].width+" "+video[i].height);
  }




  numPixels = video[0].width*video[0].height;
  prevFrame = new color[NCAM][numPixels];



}


void draw() {
  background(rgb[0]);  

  // For each camera
  for(int c = 0; c<NCAM; c++){
	  if(video[c].available()){
		  video[c].read();
		  video[c].loadPixels();
		  float sum1 = 0;
		  float sum2 = 0;
		  int dif = 0;
		  for(int i=0; i<numPixels; i++){
			color curr = video[c].pixels[i];	
			color prev = prevFrame[c][i];
	
			dif = int(abs(red(curr)-red(prev)) + abs(green(curr)-green(prev)) + abs(blue(curr)-blue(prev)));

			if(i%videoWidth<videoWidth/2){
				sum1 += dif;
			}
			else{
				sum2 += dif;
			}
	
			prevFrame[c][i] = curr;
		  }
	
		  int i = c*NCAM;
		 targetMix[i] = constrain(10.0*sum1/(255.0*255*255)-0.25,0,1);
		 targetMix[i+1] = constrain(10.0*sum2/(255.0*255*255)-0.25,0,1);
		 println(targetMix[i]);
		 /*
	  	for(int i=c*NCAM; i<c*NCAM+N/NCAM; i++){
		 targetMix[i] = constrain(10.0*sum/(255.0*255*255),0,1);
//		 println(targetMix[i]);
		}
		*/
		  video[c].updatePixels();
	
	  }

  }


  /*
  	targetMix[0] = constrain(targetMix[0]-0.1,0,1);
  	targetMix[1] = constrain(targetMix[1]-0.1,0,1);
	*/

  	for(int i=0; i<N; i++){
		mix[i] = lerp(mix[i],targetMix[i],0.15);
		rgb[i] = lerpColor(c1,c2,mix[i]);
	}


	updateRGB();
}

void mousePressed() {
	doSequenceUp = true;
}


// Send OSC messages according to rgb data
void updateRGB(){
	OscMessage myMessage;
	for(int i=0; i<N; i++){
		myMessage = new OscMessage(rgbAddr[i*3]);
		myMessage.add(map(red(rgb[i]),0,255,0,1));
		oscP5.send(myMessage, myRemoteLocation);

		myMessage = new OscMessage(rgbAddr[i*3 + 1]);
		myMessage.add(map(green(rgb[i]),0,255,0,1));
		oscP5.send(myMessage, myRemoteLocation);

		myMessage = new OscMessage(rgbAddr[i*3 + 2]);
		myMessage.add(map(blue(rgb[i]),0,255,0,1));
		oscP5.send(myMessage, myRemoteLocation);

	}



}


