import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

int qlcInputPort;
int qlcOutputPort;

String [] rgbAddr = new String[6];

color rgb;
color rgb2;
color c1;
color c2;

float mix;

boolean doSequenceUp = false;

void setup() {
  qlcInputPort = 9000;
  qlcOutputPort = 7700;
  size(600,600);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,qlcInputPort);
  myRemoteLocation = new NetAddress("127.0.0.1",qlcOutputPort);

  rgbAddr[0] = "/1/fader1";
  rgbAddr[1] = "/1/fader2";
  rgbAddr[2] = "/1/fader3";
  rgbAddr[3] = "/2/fader4";
  rgbAddr[4] = "/2/fader5";
  rgbAddr[5] = "/2/fader6";

  rgb = color(0,0,0);
  rgb2 = color(0,0,0);

  c1 = color(50,20,10);
  c2 = color(220,180,0);

  mix = 0;

  

}


void draw() {
  background(0);  

  if(doSequenceUp){
	  if(mix>=1.0){
		doSequenceUp = false;
		mix = 0;
	  }
	  else{
		rgb = lerpColor(c1,c2,mix);
		updateRGB();
	  	mix += 0.005;
	  }
  }

  else{
	mix = map(mouseX,0,width,0,1);
	rgb = lerpColor(c1,c2,mix);
	rgb2 = lerpColor(c2,c1,mix);
	updateRGB();
  }
}

void mousePressed() {
	doSequenceUp = true;
}


// Send OSC messages according to rgb data
void updateRGB(){

  OscMessage myMessage = new OscMessage(rgbAddr[0]);
  myMessage.add(map(red(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[1]);
  myMessage.add(map(green(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[2]);
  myMessage.add(map(blue(rgb),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[3]);
  myMessage.add(map(red(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[4]);
  myMessage.add(map(green(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

  myMessage = new OscMessage(rgbAddr[5]);
  myMessage.add(map(blue(rgb2),0,255,0,1));
  oscP5.send(myMessage, myRemoteLocation);

}


/* incoming osc message are forwarded to the oscEvent method. 
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage 
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());
  println(" typetag: "+theOscMessage.get(0).floatValue());
}
*/
